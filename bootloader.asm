[org 0x7C00]
[bits 16]

; Init segments
cli
cld
jmp 0:main
main:
xor ax, ax
mov es, ax
mov ds, ax
mov fs, ax
mov gs, ax
mov ss, ax
mov sp, 0x7BFF
mov bp, sp

; A20
call check_a20
cmp ax, 1
je a20_ok
mov ax, 0x2401
int 0x15
call check_a20
cmp ax, 1
je a20_ok
no_a20: hlt
jmp no_a20

a20_ok:
cli ; Disable interrupts

; Zero memory
xor al, al
mov ecx, 0x4000
mov edi, 0x1000
rep stosb
; Create page table at 0x1000
mov eax, 0x1000
mov ebx, 0x00
pt_loop:
    call create_pt_entry
    add eax, 0x08
    add ebx, 0x1000
    cmp eax, 0x2000
    jne pt_loop
; Create page directory at 0x2000
mov ebx, 0x1000
call create_pt_entry
; Create page directory pointer table at 0x3000
mov ebx, 0x2000
mov eax, 0x3000
call create_pt_entry
; Create page map level 4 at 0x4000
mov ebx, 0x3000
mov eax, 0x4000
call create_pt_entry

; Set page table
mov cr3, eax

; Enable PAE
mov eax, cr4
or eax, 1 << 5
mov cr4, eax

; Switch to long mode
mov ecx, 0xC0000080
rdmsr
or eax, 1 << 8
wrmsr

lgdt [gdt.descriptor] ; Load GDT

mov eax, cr0
or eax, 1 << 31 | 1
mov cr0, eax

jmp CODE_SEG:init_lm

[bits 64]
init_lm:

; Divider
halt: hlt
jmp halt
; Library functions

[bits 16]
halt_16: hlt
jmp halt_16

; Paging
; eax: physical address
; ebx: page address
create_pt_entry:
    push ebx
    or ebx, 0x03
    mov [eax], ebx
    xor ebx, ebx
    mov [eax + 4], ebx
    pop ebx
    ret
[bits 64]

; GDT
gdt:
    .null:
        dq 0x00
    .code:
        dw 0xFFFF ; Limit bits 15:0
        dw 0x0000 ; Base bits 15:0
        db 0x00 ; Base bits 23:16
        db 0b10011010 ; Access bits
        db (0b111 << 5) | 0xF ; Flags and limit bits 19:16
        db 0x00 ; Base bits 31:24
.end:
.descriptor:
    dw .end - .null - 1
    dd .null
CODE_SEG equ .code - .null

[bits 16]
check_a20:
    pushf
    push ds
    push es
    push di
    push si
    cli
    xor ax, ax ; ax = 0
    mov es, ax
    not ax ; ax = 0xFFFF
    mov ds, ax
    mov di, 0x0500
    mov si, 0x0510
    mov al, byte [es:di]
    push ax
    mov al, byte [ds:si]
    push ax
    mov byte [es:di], 0x00
    mov byte [ds:si], 0xFF
    cmp byte [es:di], 0xFF
    pop ax
    mov byte [ds:si], al
    pop ax
    mov byte [es:di], al
    mov ax, 0
    je .exit
    mov ax, 1
.exit:
    pop si
    pop di
    pop es
    pop ds
    popf
    ret

times 0x1FE - ($-$$) db 0
dw 0xAA55
